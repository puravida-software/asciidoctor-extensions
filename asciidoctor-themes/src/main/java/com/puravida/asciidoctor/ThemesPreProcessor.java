package com.puravida.asciidoctor;

import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Preprocessor;
import org.asciidoctor.extension.PreprocessorReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.Map;

public class ThemesPreProcessor extends Preprocessor{

    public ThemesPreProcessor(){
    }

    protected static final String STYLEDIR = "custom-theme";

    @Override
    public void process(Document document, PreprocessorReader reader) {

        document.getAttributes().put("linkcss", "");
        document.getAttributes().put("stylesdir", STYLEDIR);

    }


}
