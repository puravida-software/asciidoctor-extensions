package com.puravida.asciidoctor.plot

import com.puravida.plot.PlotBuilder
import groovy.util.logging.Log
import org.asciidoctor.ast.StructuralNode
import org.asciidoctor.extension.BlockProcessor
import org.asciidoctor.extension.Reader


@Log
abstract class AbstractPlotProcessor extends BlockProcessor{

    AbstractPlotProcessor(String name, Map<String, Object> config) {
        super(name, [contexts: [':literal'], content_model: ':simple'])
    }

    def process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
        String rootDir = parent.document.options.find{"$it.key"=='to_dir'}.value ?: '.'
        File fRootDir = new File(rootDir)
        if( !fRootDir.isDirectory() )
            fRootDir=fRootDir.parentFile

        String imagedir = parent.document.attributes['imagesdir'] ?: ''
        File fImageDir = new File("$fRootDir.absolutePath/$imagedir")
        fImageDir.mkdirs()

        String filename = attributes.get(2L) ?: "${new Date().time}.png"
        filename=parent.normalizeWebPath(filename,"",true)

        int width = (attributes.get(3L) ?: "800") as int
        int height = (attributes.get(4L) ?: "600") as int

        plotFunction(reader,"$fImageDir.absolutePath/$filename",width,height)

        Map<String,Object> attr = [:]
        attr.target = "$filename".toString()
        attr.alt = "$filename".toString()
        attr.width =  "$width".toString()
        attr.height =  "$height".toString()


        Map<Object,Object>options= [:]

        createBlock(parent,"image","", attr,options)
    }

    abstract ByteArrayOutputStream executeClosure(Closure closure)

    void plotFunction(Reader reader, String filename, int w, int h){
        try {
            String dsl = reader.lines().join('\n')
            Closure closure = new GroovyShell().evaluate("{-> $dsl }") as Closure
            ByteArrayOutputStream bos = executeClosure(closure)
            new File(filename) << bos.toByteArray()
        }catch(e){
            e.printStackTrace()
            log.severe "$filename: ${e.toString()}"
        }
    }

}
