package com.puravida.asciidoctor.plot

import org.asciidoctor.OptionsBuilder
import org.junit.Before
import org.junit.Test
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;

class TestPlotProcessor {

    @Before
    void setup(){
        new File('build').mkdirs()
    }

    @Test
    public void postProcessorIsAppliedToPdf() {
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format("""= My document

Hello World

[lineplot]
....
title 'Simple LinePlot'
legendOrientation horizontal
xAxis.from (-10) to 10 increment 0.02
xAxis.label 'Eje X'
yAxis.label 'Eje Y'
series.'D' = evaluate 'cos(x)'
....

"""), OptionsBuilder.options()
                .toDir(new File('build'))
                .toFile(new File('testplot.pdf'))
                .backend('pdf'));
    }

    @Test
    public void postProcessorIsAppliedToHtml() {
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format("""= My document

Hello World

.LinePlot
[lineplot]
....
title 'Simple LinePlot'
legendOrientation horizontal
xAxis.from (-10) to 10 increment 0.02
xAxis.label 'Eje X'
yAxis.label 'Eje Y'
series.'D' = evaluate 'cos(x)'
....


[barplot]
....
title 'Simple Barplot'
legendVisible false
xAxis.from 1 to 8
series.B = serie([1,3,-2,6,-4,9,8,11]).using(['a','b','c','d','e','f','g','h'])
....

"""), OptionsBuilder.options()
                .toDir(new File('build'))
                .toFile(new File('testplot.html')).backend('html'));
    }


    @Test
    public void gnuPostProcessorIsAppliedToHtml() {
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format("""= My document

Hello World

[gnuplot,gnuplot.png,800,600]
....
plot [-4:4] exp(-x**2 / 2)
....

"""), OptionsBuilder.options()
                .toDir(new File('build'))
                .toFile(new File('testgnuplot.html')).backend('html'));
    }

}
