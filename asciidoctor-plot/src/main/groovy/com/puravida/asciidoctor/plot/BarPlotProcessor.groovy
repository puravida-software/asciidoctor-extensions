package com.puravida.asciidoctor.plot

import com.puravida.plot.PlotBuilder

class BarPlotProcessor  extends AbstractPlotProcessor{

    BarPlotProcessor(String name, Map<String, Object> config) {
        super(name,config)
    }

    ByteArrayOutputStream executeClosure(Closure closure){
        PlotBuilder.instance.barPlot  closure
    }

}
