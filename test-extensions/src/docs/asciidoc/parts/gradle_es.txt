
[role='language-es']
=== Gradle

Añade el repositorio y las dependencias de PuraVida en Bintray en el fichero build.gradle

[#repositories,source]
----
repositories {
    mavenCentral()
    // others
}
----

[#dependencies,source,subs="attributes"]
----
dependencies {
    asciidoctor "com.puravida-software.asciidoctor:asciidoctor-extensions:{extensions-version}"
}
----
