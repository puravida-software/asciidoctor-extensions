= Puravida Extensions
Jorge Aguilera
:doctype: book
:toc: left
:toc3:
:google-analytics-code: UA-687332-11

[[abstract]]

Some usefull (I hope) extensions to http://wwww.asciidoctor.org[Asciidoctor]

== Install

Add PuraVida Bintray repository in your build.gradle

[source]
----
repositories {
    maven {
        url  "http://dl.bintray.com/puravida-software/repo"
    }
    // others
}
----

Include the extension

[source]
----
dependencies {
    asciidoctor "com.puravida.asciidoctor:asciidoctor-extensions:1.1"
}
----

== Google Analytics

If you put the attribute *google-analytics-code* in the header of your document
it will render a javascript code at the end of the HTML with your
Google Analytics code

[source]
----
= My document
Author
:doctype: book
:google-analytics-code: UA-000000-00
----

....
<!-- google -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-000000-00', 'auto');
ga('send', 'pageview');
</script>
....


== Disqus

- Open an account at https://disqus.com/ (if you haven't yet)

- Create a site, i.e. "asciidoctor-extensions"

- Create a *disqus* inline block in your documentation with the name of your recently site created:

[source]
----
disqus::asciidoctor-extensions[] //<1>
----
<1> replace asciidoctor-extensions with you site

Next, you can see the result:

disqus::asciidoctor-extensions[]


