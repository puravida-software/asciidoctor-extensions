= My multilanguage document
Jorge Aguilera
:toc: left
:toclevels: 4
:imagesdir: images
:multilanguage: gb,es,it

This parragrah will be always visible. Este párrafo será siempre visible

We want a toolbar / Queremos una barra de idiomas

multilanguage::toolbar[]

[role='language-gb']
This paragrah is writen in english and it'll be only visible if `language is equal to gb`

[role='language-es']
Este párrafo está escrito en español y solo será visible si `language vale es`


[role='language-it']
== Il Titolo

tutta questa sezione

sarà visibile solo

quando selezioni

l'italiano

[role='language-es']
== El titulo

toda esta seccion

será visible únicamente

cuando se seleccione

español



