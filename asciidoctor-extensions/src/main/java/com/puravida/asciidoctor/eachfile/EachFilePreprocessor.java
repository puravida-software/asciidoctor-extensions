package com.puravida.asciidoctor.eachfile;

import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Preprocessor;
import org.asciidoctor.extension.PreprocessorReader;

import java.io.File;
import java.io.FileFilter;
import java.util.*;
import java.util.regex.Pattern;

public class EachFilePreprocessor extends Preprocessor {

    public EachFilePreprocessor(Map<String, Object> config) {
        super(config);
    }

    enum status{
        outside,
        detected,
        into
    };

    void includeFiles(Document document, String header, List<String> template, List<String> lines  ){

        final String[]headers = header.split(",");
        Map<String,String>args = new HashMap<String,String>();
        for(String str : headers){
            String[] tmp = str.split("=");
            args.put(tmp[0], tmp.length > 1 ? tmp[1] : null);
        }

        // file pattern
        final String pattern = args.get("pattern");
        if( pattern == null){
            System.err.println("No pattern provided for each-file extension");
            return;
        }

        File[] pagesTemplates = new File[0];
        String currentDirectory = new File(".").getAbsolutePath();
        currentDirectory=currentDirectory.substring(0,currentDirectory.length()-1);

        Object base_dir = document.getOptions().get("base_dir");
        File baseDir = new File( base_dir != null ? base_dir.toString() : currentDirectory );

        // subdirectory
        final String subdir = args.get("dir");
        if( subdir != null ){
            baseDir = new File(baseDir,subdir);
        }

        final Pattern p = Pattern.compile(pattern);
        pagesTemplates = baseDir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if( f.isDirectory() )
                    return false;
                boolean ret = p.matcher(f.getName()).matches();
                return ret;
            }
        });
        pagesTemplates = pagesTemplates != null ? pagesTemplates : new File[0];

        final String sortBy=args.get("sort");
        final boolean asc = args.get("order") == null || "asc".equalsIgnoreCase(args.get("order"));
        if( sortBy != null ){
            Arrays.sort(pagesTemplates, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    switch( sortBy ){
                        case "date":
                            return Long.valueOf(o1.lastModified()).compareTo(Long.valueOf(o2.lastModified()))*(asc ? 1 : -1);
                        case "size":
                            return Long.valueOf(o1.length()).compareTo(Long.valueOf(o2.length()))*(asc ? 1 : -1);
                        case "name":
                        default:
                            return o1.getName().compareTo(o2.getName())*(asc ? 1 : -1);
                    }
                }
            });
        };

        for(File f : pagesTemplates){
            for(String s: template){
                lines.add(s.replaceAll("\\{filename\\}",f.getName())+"\n");
            }
        }
    }

    @Override
    public void process(Document document, PreprocessorReader reader) {
        // Read the full document
        List<String> lines = reader.readLines();

        List<String> newLines = new ArrayList<>();

        status status = EachFilePreprocessor.status.outside;

        String header=null;
        String delimiterBlock=null;
        List<String> template = null;

        for(int i=0; i<lines.size(); i++){
            String line = lines.get(i);

            if ( status == EachFilePreprocessor.status.outside ) {
                if (line.startsWith("[each-file") && line.endsWith("]")) {
                    header = line.substring(1,line.length()-1);
                    template = new ArrayList<>();
                    status = EachFilePreprocessor.status.detected;
                    continue;
                }
            }

            if( status == EachFilePreprocessor.status.detected){
                delimiterBlock = line;
                status = EachFilePreprocessor.status.into;
                continue;
            }

            if( status == EachFilePreprocessor.status.into && line.equals(delimiterBlock)){
                includeFiles(document, header, template, newLines);
                header = delimiterBlock = null;
                status = EachFilePreprocessor.status.outside;
                continue;
            }

            if( status == EachFilePreprocessor.status.into ){
                template.add(line);
                continue;
            }

            if (line.trim().equals("")) {
                newLines.add(" ");
                continue;
            }
            newLines.add(line);
        }

        Map<String, Object> attributes = new HashMap<>();
        for (int i = newLines.size() - 1; i >= 0; i--) {
            reader.push_include(newLines.get(i), null, null, 1, attributes);
        }

    }
}
