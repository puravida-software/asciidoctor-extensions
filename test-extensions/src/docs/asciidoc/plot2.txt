[barplot]
....
title 'Simple Barplot'
legendVisible false
xAxis.from 1 to 8
series.B = serie([1,3,-2,6,-4,9,8,11]).using(['a','b','c','d','e','f','g','h'])
....
